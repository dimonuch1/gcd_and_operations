import UIKit
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

// .qso:
// .background - 9
// .userInitiated - 25
// .userInteractive - 25
//
//



//var safeString = ThreadSafeString("")
//var usualString = ""


//var view = UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
//var image = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
//
//image.backgroundColor = .black
//image.contentMode = .scaleAspectFit
//view.addSubview(image)
//
//PlaygroundPage.current.liveView = view
//
//func fetchImage() {
//    let imageUrl: URL = URL(string: "http://www.planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!
//
//    let queue = DispatchQueue.global(qos: .utility)
//    queue.async {
//        if let data = try? Data(contentsOf: imageUrl) {
//            DispatchQueue.main.async {
//                image.image = UIImage(data: data)
//                print("Show image data")
//            }
//            print("Did download image data")
//        }
//    }
//
//}

//fetchImage()

//PlaygroundPage.current.finishExecution()


// ####   Глобальные очереди и задания   ####


fileprivate var lastStartTime = Date()

public func startClock(){
  lastStartTime = Date()
}

public func stopClock() -> TimeInterval {
  return Date().timeIntervalSince(lastStartTime)
}


let mainQueue = DispatchQueue.main


let userInteractiveQueue = DispatchQueue.global(qos: .userInteractive)
let userQueue = DispatchQueue.global(qos: .userInitiated)
let utilityQueue = DispatchQueue.global(qos: .utility)
let backgroundQueue = DispatchQueue.global(qos: .background)

let defaultQueue = DispatchQueue.global() //default

func task(_ symbol: String) {
    for i in 1...9 {
        print("\(symbol) \(i) приоритет = \(qos_class_self().rawValue)")
    }
}

func taskHigh(_ symbol: String) {
        print("\(symbol) HIGH приоритет = \(qos_class_self().rawValue)")
}

//print("-------------------------------------------")
//print("Синхронность sync")
//print("Global .concurent Q1 - .userInitiated")
//print("-------------------------------------------")
//
//userQueue.sync {
//    task("😎")
//}
//task("😈")
//
//sleep(2)
//
//print("-------------------------------------------")
//print("Асинхронность async")
//print("Global .concurent Q1 - .userInitiated")
//print("-------------------------------------------")
//
//userQueue.async {
//    task("😎")
//}
//task("😈")
//
//sleep(2)

//let mySerialQueue = DispatchQueue(label: "com.test") //по умолчанию создается .serial
//
//print("-------------------------------------------")
//print("Синхронность sync")
//print("Private .serial Q1 - no")
//print("-------------------------------------------")
//
//mySerialQueue.sync {
//    task("😎")
//}
//task("😈")
//
//sleep(2)
//
//print("-------------------------------------------")
//print("Асинхронность async")
//print("Private .serial Q1 - no")
//print("-------------------------------------------")
//
//mySerialQueue.async {
//    task("😎")
//}
//task("😈")
//
//sleep(2)

//print("-------------------------------------------")
//print("Private .serial Q1 - .userInitiated")
//print("-------------------------------------------")
//
//let serialPriorityQueue = DispatchQueue(label: "com.test", qos: .userInitiated)
//let serialPriorityQueue2 = DispatchQueue(label: "com.test", qos: .background)
//serialPriorityQueue.asyncAfter(deadline: .now() + 0.1, qos: .userInteractive) {
//    task("😎")
//}
//serialPriorityQueue2.async { task("😈") }

//print("-------------------------------------------")
//print("Private .concurent Q1 - .userInitiated")
//print("-------------------------------------------")
//
//let workerQueue = DispatchQueue(label: "com.test", qos: .default, attributes: [.concurrent, .initiallyInactive])
//workerQueue.async { task("😎") }
//workerQueue.async { task("😈") }
//
//workerQueue.activate()

//print("-------------------------------------------")
//print("DispatchWorkItem")
//print("-------------------------------------------")
//
//let highPriorityItem = DispatchWorkItem(qos: .userInteractive, flags: [.enforceQoS]) {
//    task("🎉")
//}
//
////safeString.setString(string: "")
////usualString = ""


//let workerQueue2 = DispatchQueue(label: "queue2", qos: .background, attributes: [.concurrent])
//let workerQueue3 = DispatchQueue(label: "queue3", qos: .background, attributes: [.concurrent])
//
//workerQueue2.async { task("😎") }
//workerQueue2.async { task("😈") }
//
//workerQueue2.async ( execute: highPriorityItem )
//workerQueue3.async ( execute: highPriorityItem )

//print("-------------------------------------------")
//print("DispatchGroup")
//print("-------------------------------------------")
//
//let group = DispatchGroup()

//for i in 0...1 {
//    DispatchQueue.global().async(group: group) {
//        print("1 - \(i) - \(qos_class_self().rawValue)")
//    }
//}
//
//for i in 2...3 {
//    DispatchQueue.global(qos: .userInteractive).async(group: group) {
//        print("2 - \(i) - \(qos_class_self().rawValue)")
//    }
//}

//group.enter()
//
//for i in 0...2 { print(i) }
//
//for i in 2...3 { print(i) }
//group.leave()
//
//group.notify(queue: DispatchQueue.main) {
//    print("finish")
//}

//let operation1 = {
//    print("operation 1 start")
//}
//
//let operation2 = {
//    print("operation 2 start")
//}
//let operation3 = {
//    print("operation 3 start")
//}
//let operation4 = {
//    print("operation 4 start")
//}
//let operation5 = {
//    print("operation 5 start")
//}
//let operation6 = {
//    print("operation 6 start")
//}
//let operation7 = {
//    print("operation 7 start")
//}
//let operation8 = {
//    print("operation 8 start")
//}
//
//class OperationTest: Operation {
//    override func main() {
//        for i in 0...10000 {
//            print("item: \(i)")
//        }
//    }
//}
//
//let testOperation = OperationTest()
//let queue = OperationQueue()
//queue.addOperation(testOperation)
//testOperation.cancel()




//let conOp = BlockOperation {
//    print("------" + "=========")
//}
//conOp.qualityOfService = .userInitiated
//
//
//let queue = OperationQueue()
//
//
//
//queue.maxConcurrentOperationCount = 2
//queue.addOperation(operation1)
//queue.addOperation(operation2)
//queue.addOperation(operation3)
//queue.addOperation(operation4)
//queue.addOperation(operation5)
//queue.addOperation(operation6)
//queue.addOperation(operation7)
//queue.addOperation(operation8)
//queue.addOperation(conOp)
//
//queue.waitUntilAllOperationsAreFinished()
//
//print("finish")
//


class AsyncOperation: Operation {

    enum State: String {
        case ready, executing, finished
        fileprivate var keyPath: String {
            return "is" + rawValue.capitalized
        }
    }

    var state = State.ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath)
            willChangeValue(forKey: state.keyPath)
        }

        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
    }
}

extension AsyncOperation {
    override var isAsynchronous: Bool {
           return true
       }

       override var isReady: Bool {
            return super.isReady && state == .ready
       }

       override var isExecuting: Bool {
            return state == .finished
       }

       override var isFinished: Bool {
            return state == .finished
       }

       override func start() {
        if isCancelled {
            state = .finished
            return
        }
           main()
           state = .executing
       }

    override func cancel() {
        state = .finished
        super.cancel()
    }
}

func asyncAdd(lhs: Int, rhs: Int, callback: @escaping (Int) -> ()) {

    URLSession.shared.dataTask(with: URL(string: "http://planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!) { (data, response, error) in
        callback(lhs + rhs)
    }.resume()


    //let additionalQueue = OperationQueue()
    //additionalQueue.addOperation {
        //sleep(1)
        //callback(lhs + rhs)
    //}
}

class SumNotSync: Operation {
    let lhs: Int
    let rhs: Int
    var result: Int?

    init(lhs: Int, rhs: Int) {
        self.lhs = lhs
        self.rhs = rhs
        super.init()
    }

    override func main() {
        asyncAdd(lhs: lhs, rhs: rhs) { (result) in
            self.result = result
        }
    }

    deinit {
        print("deinit NOT Sync")
    }

    override func cancel() {
        print("NOT async call cancel")
    }
}

class SumOperation: AsyncOperation {
    let lhs: Int
    let rhs: Int
    var result: Int?

    init(lhs: Int, rhs: Int) {
        self.lhs = lhs
        self.rhs = rhs
        super.init()
    }

    override func main() {
        asyncAdd(lhs: lhs, rhs: rhs) { (result) in
            self.result = result
            self.state = .finished
        }
    }

    deinit {
        print("deinit SumOperation")
    }
}

let additionalQueue = OperationQueue()

let input = [(3,4),(5,6),(6,7),(7,8),(8,9),(1,9)]

print("start")

//for (lhs,rhs) in input {
//
//    let operation = SumOperation(lhs: lhs, rhs: rhs)
//    operation.completionBlock = {
//        guard let result = operation.result else {
//            return
//        }
//        print("\(lhs) + \(rhs) = \(result)")
//    }
//
//    additionalQueue.addOperation(operation)
//}

func asyncImageLoad(url: URL, callback: @escaping (UIImage?, Bool) -> ()) {
    // URL(string: "http://planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")
    URLSession.shared.dataTask(with: url) { (data, response, error) in
        if let data = data {
            callback(UIImage(data: data), false)
        }
        callback(nil, true)
    }.resume()
}

class ImageLoadOperation: AsyncOperation {
    var url: URL?
    var outputImage: UIImage?

    init(url: URL) {
        self.url = url
        //self.url = URL(string: "http://planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!
        super.init()
    }

    override func main() {
        if let imageUrl = url {
            asyncImageLoad(url: imageUrl) { [weak self] (image, isError) in
                if !isError {
                    print("i was loaded image: \(image)")
                    self?.outputImage = image
                    self?.state = .finished
                } else {
                    self?.state = .finished
                }
            }
        }
    }
}

//передача данных в зависимую операцию.
protocol ImagePass {
    var image: UIImage? { get }

}

extension ImageLoadOperation: ImagePass {
    var image: UIImage? { return outputImage }
}

//операция фильтрации
class ImageTakeOperation: Operation {
    var outputImage: UIImage?
    private let _inputImage: UIImage?

    init(_ image: UIImage?) {
        _inputImage = image
        super.init()
    }

    var inputImage: UIImage? {
        // определяем задан ли у операции inputImage
        // если НЕТ, то анализируем dependecies
        // поторые "подтверждают" протокол Imagepass

        var image: UIImage?
        if let inputImage = _inputImage {
            image = inputImage
        } else if let dataProvider = dependencies
            .filter({$0 is ImagePass})
            .first as? ImagePass {
            image = dataProvider.image
        }
        return image
    }

    override func main() {
        //имитируем обработку изображения
        print("make ImageTakeOperation: \(inputImage)")
        outputImage = inputImage
    }

}

let imageLoader = ImageLoadOperation(url: URL(string: "http://planetware.com/photos-large/F/france-paris-eiffel-tower.jpg")!)
let filter = ImageTakeOperation(nil)

filter.addDependency(imageLoader)

//let queue = OperationQueue()
//queue.addOperations([imageLoader, filter], waitUntilFinished: true)

func slowAdd(_ input: (Int,Int)) -> Int {
    sleep(1)
    return input.0 + input.1
}

class ArraySumOperation: Operation {
    let inputArray: [(Int, Int)]
    var outputArray = [Int]()

    init(input: [(Int, Int)]) {
        self.inputArray = input
        super.init()
    }

    override func main() {
        for pair in inputArray {
            if isCancelled { return }
            outputArray.append(slowAdd(pair))
        }
    }
}

let numberArray = [(1,3),(3,5),(5,7),(7,9),(2,8)]

let sumOperation = ArraySumOperation(input: numberArray)
let queue = OperationQueue()

startClock()
//queue.addOperation(sumOperation)

//sleep(2)
sumOperation.cancel()

sumOperation.completionBlock = {
    stopClock()
    sumOperation.outputArray
    PlaygroundPage.current.finishExecution()
}

public func slowAddArray(_ input: [(Int, Int)],
                         progress: ((Double) -> (Bool))? = nil) -> [Int] {
  var results = [Int]()
  for pair in input {
    results.append(slowAdd(pair))
    if let progress = progress {
      if !progress(Double(results.count) / Double(input.count)) { return results }
    }
  }
  return results
}


class Filter: ImageTakeOperation {
    override func main() {
        if isCancelled { return }
        guard let inputImage = inputImage else { return }

        if isCancelled { return }
        //outputImage = inputImage.appl
    }
}





//////////////////////////-----------------------------------------------------------------------------------------------------
protocol BaseProtocol {
    var string: String? { get }
}

class BaseOperation: Operation {
    var outputString: String?
    private var _inputString: String?

    init(string: String?) {
        _inputString = string
        super.init()
    }

    var inputStirng: String? {
        var string: String?
        if let inputString = _inputString {
            string = inputString
        } else if let dataProvider = dependencies
        .filter({ $0 is BaseProtocol})
            .first as? BaseProtocol {
            print("dependencies: \(dataProvider.string)")
            string = dataProvider.string
        }
        return string
    }
}

extension BaseOperation: BaseProtocol {
    var string: String? {
        return outputString
    }
}

class FirstOperation: Operation {
    private var myString = ""

    init(string: String) {
        self.myString = string
        super.init()
    }

    override func main() {
        myString += "FirstOperation"
        print("FirstOperation: \(myString)")
    }
}

extension FirstOperation: BaseProtocol {
    var string: String? {
        return self.myString
    }
}

class SecondOperation: BaseOperation {
    override func main() {
        let str = " + SecondOperation"
        outputString = (inputStirng ?? "") + str
        print("SecondOperation: \(outputString)")
    }
}

class ThreeOperation: BaseOperation {

    private let completion: (_ string: String?) -> ()

    init(completion: @escaping (String?) -> ()) {
        self.completion = completion
        super.init(string: nil)
    }

    override func main() {
        let str = " + ThreeOperation"
        outputString = (inputStirng ?? "") + str
        print("ThreeOperation: \(outputString)")
        completion(outputString)
    }
}

let first = FirstOperation(string: "")
let two = SecondOperation(string: nil)
let three = ThreeOperation { (string) in
    print("Final: \(string)")
}

let operations = [first, two, three]

two.addDependency(first)
three.addDependency(two)

let operationQueue = OperationQueue()
operationQueue.addOperations(operations, waitUntilFinished: false)

three.completionBlock = {
    print("outputString = \(three.outputString)")
}
